/**
 * Created by Yury.Michurin on 23/11/2014.
 */
/*global module,require*/
var FacebookProvider = require("./providers/FacebookProvider"),
    fs = require("fs"),
    basePath = "./mockdata/",
    Config = require("./config"),
    Promise = require('es6-promise').Promise;

var facebookProvider = new FacebookProvider();

var postsP = facebookProvider.getPosts("lan2lan.statushunter");

postsP.then(function (data) {
    data.data.forEach(function (post) {
        facebookProvider.getComments(post.id).then(function (comments) {
            fs.writeFile(basePath + "comments_" + post.id + ".json", JSON.stringify(comments));
        }, function (err) {
            console.log("Error getting comments", err);
        });
    });
    fs.writeFile(basePath + "posts.json", JSON.stringify(data));
}, function (err) {
    console.log("Error getting posts", err);
});
