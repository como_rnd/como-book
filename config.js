/**
 * Created by Yury.Michurin on 23/11/2014.
 */
/*global module,require*/

module.exports = {
  "facebookAccessToken": "CAACEdEose0cBAKtONRaH3vqyZBKWUbs8BRbbSL0b2KuOqp8DqAMNHbSFHDNfasHC5DQinhtMMTq6wkcgbbMKNMXvtgedS5yR7DjtxrYL0jOW7odj6IYkbUzspAZCeMP6YcqVOMvZAZBSOpnKZBZAOcjn1y4prvuMZAHYqrQdqo3cONngvRV2ECoih7xiN9VZAhlC6vk2ZAxF3uGQBjwhSsZAZBR",
  "httpPort": process.env.PORT,
  "httpHost": process.env.IP
};