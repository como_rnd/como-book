/**
 * Created by Yury.Michurin on 23/11/2014.
 */
/*global module,require*/
var React = require("react"),
    debug = require("debug")("FacebookPost"),
    FacebookComments = require("./FacebookComments"),
    FacebookPost;

FacebookPost = React.createClass({
    "getInitialState": function FacebookPost_getInitialState() {
        debug("getInitialState");
        return null;
    },
    "componentWillMount": function FacebookPost_componentWillMount() {
        debug("componentWillMount");
    },
    "render": function FacebookPost_render() {
        debug("render");

        return React.createElement("div", { className: "facebookPost" },
            React.createElement("p", null, this.props.post.message),
            React.createElement(FacebookComments, {postId: this.props.post.id})
        );
    }
});

module.exports = FacebookPost;