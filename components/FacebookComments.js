/**
 * Created by Yury.Michurin on 23/11/2014.
 */
/*global module,require*/
var React = require("react"),
    debug = require("debug")("FacebookComments"),
    FacebookComment = require("./FacebookComment"),
    FacebookComments;

FacebookComments = React.createClass({
    "getInitialState": function FacebookComments_getInitialState() {
        debug("getInitialState");
        /***CHANGE_ME***/
        return {
            "comments": JSON.parse(require("fs").readFileSync("./mockData/comments_" + this.props.postId + ".json")).data
        };
        /***CHANGE_ME***/
    },
    "componentWillMount": function FacebookComments_componentWillMount() {
        debug("componentWillMount");
    },
    "render": function FacebookComments_render() {
        debug("render");

        if (!this.state.comments) {
            return React.createElement("div", {className: "spinner"});
        }

        return React.createElement("ul", { className: "facebookComments" }, this.state.comments.map(function (comment) {
            return React.createElement("li", { key: comment.id }, React.createElement(FacebookComment, { comment: comment }));
        }));
    }
});

module.exports = FacebookComments;