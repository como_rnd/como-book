/**
 * Created by Yury.Michurin on 23/11/2014.
 */
/*global module,require*/
var React = require("react"),
    debug = require("debug")("FacebookComment"),
    FacebookComment;

FacebookComment = React.createClass({
    "getInitialState": function FacebookComment_getInitialState() {
        debug("getInitialState");
        return null;
    },
    "componentWillMount": function FacebookComment_componentWillMount() {
        debug("componentWillMount");
    },
    "render": function FacebookComment_render() {
        debug("render");
        return React.createElement("blockquote", { className: "facebookComment" },
                React.createElement("time", null, this.props.comment.created_time),
                React.createElement("p", null, this.props.comment.message),
                React.createElement("cite", null,
                    React.createElement("a", { href: "https://www.facebook.com/" + this.props.comment.from.id }, this.props.comment.from.name)
                )
        );
    }
});

module.exports = FacebookComment;