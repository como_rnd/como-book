/**
 * Created by Yury.Michurin on 23/11/2014.
 */
/*global module,require*/
var React = require("react"),
    debug = require("debug")("FacebookPage"),
    FacebookPost = require("./FacebookPost"),
    FacebookPage;

FacebookPage = React.createClass({
    "getInitialState": function FacebookPage_getInitialState() {
        debug("getInitialState");

        /***CHANGE_ME***/
        return {
            "posts": JSON.parse(require("fs").readFileSync("./mockData/posts.json")).data
        };
        /***CHANGE_ME***/
    },
    "componentWillMount": function FacebookPage_componentWillMount() {
        debug("componentWillMount");
    },
    "render": function FacebookPage_render() {
        debug("render");

        if (!this.state.posts) {
            return React.createElement("div", {className: "spinner"});
        }

        return React.createElement("ul", { className: "facebookPage" },
                    this.state.posts.map(function (post) {
                        return React.createElement("li", { key: post.id }, React.createElement(FacebookPost, { post: post }));
                    })
        );
    }
});

module.exports = FacebookPage;