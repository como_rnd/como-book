/**
 * Created by Yury.Michurin on 23/11/2014.
 */
/*global module,require*/
var request = require("request"),
    Promise = require("es6-promise").Promise,
    debug = require("debug")("FacebookProvider"),
    Config = require("../config");

function FacebookProvider() {}

/* Statics */
FacebookProvider.BaseURL = "https://graph.facebook.com/v2.2/";

/* Instance privates */
FacebookProvider.prototype._makeApiCall = function FacebookProvider__makeApiCall(endPoint) {
    var self = this;
    return new Promise(function (resolve, reject) {
        var requestUrl = FacebookProvider.BaseURL + endPoint + "?access_token=" + Config.facebookAccessToken;
        debug("requesting " + requestUrl);
        request({
            "url": requestUrl,
            "json": true,
            "method": "GET"
        }, function loadPostsHandler(error, response, body) {
            if (error) {
                reject(new Error(error));
            } else {
                resolve(body);
            }
        });
    });
};

/* Instance publics */
FacebookProvider.prototype.getPosts = function FacebookProvider_getPosts(pageId) {
    return this._makeApiCall(pageId + "/posts");
};

FacebookProvider.prototype.getComments = function FacebookProvider_getComments(postId) {
    return this._makeApiCall(postId + "/comments");
};

module.exports = FacebookProvider;