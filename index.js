/**
 * Created by Yury.Michurin on 23/11/2014.
 */
/*global module,require*/
var React = require("react"),
    fs = require("fs"),
    template = fs.readFileSync("./public/index.html").toString(),
    templateCss = fs.readFileSync("./public/style.css").toString(),
    http = require('http'),
    server = null,
    FacebookPage = require("./components/FacebookPage"),
    Config = require("./config"),
    debug = require("debug")("index");

function renderHtml() {
    var element,
        html;

    element = React.createElement(FacebookPage,  { name: "lan2lan.StatusHunter" });
    html = template.replace("<!--REACT_OUTPUT-->", React.renderToString(element));



    return html;
}

server = http.createServer(function requestHandler(req, res) {
    debug("Incoming req " + req.url + " from " + req.connection.remoteAddress);
    if (req.url === "/favicon.ico") {
        res.writeHead(418, { "Content-Type": "text/plain"});
        res.write("I'm a teapot. Srsly.");
        res.end();
        return;
    }

    if (req.url === "/style.css") {
        res.writeHead(200, { "Content-Type": "text/css"});
        res.write(templateCss);
        res.end();
        return;
    }

    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.write(renderHtml());
    res.end();


});

server.listen(Config.httpPort, Config.httpHost);